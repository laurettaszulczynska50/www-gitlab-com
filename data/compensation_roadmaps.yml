- slug: backend-engineer
  name: Backend Engineer
  role_path: /job-families/engineering/backend-engineer/
  grades:
    materials:
      grade: B
      comment: The description and career development materials need to be unified. The title is fine.
    recruiting:
      grade: B
      comment: As of April 2019 we are at 70% of our hiring plan
    employee_satisfaction:
      grade: A
      comment: There are little-to-no complaints after the October 2018 compensation change
    external_comparables:
      grade: A
      comment: We are within 15% of our external compensation data
    internal_comparables:
      grade: A
      comment: This benchmark is at or above those of comparable roles
    bonus:
      grade: C
      comment: This role has incentive bonuses at the Distinguished/Director level and above, but not at the Staff/Manager level
  recommendation:
    date: 2019-04-02
    recommendations:
      - A cost-of-living increase in the next compensation review.
      - We will try to make the case for an incentive bonus for the Staff/Manager level before the end of 2019.
- slug: frontend-engineer
  name: Frontend Engineer
  role_path: /job-families/engineering/frontend-engineer/
  grades:
    materials:
      grade: B
      comment: The description and career development materials need to be unified. The title is fine.
    recruiting:
      grade: C
      comment: We are seeing a high rise in offers declined due to low compensation
    employee_satisfaction:
      grade: C
      comment: There is sensitivity around the Backend Engineer benchmark being raised in October 2018 because it is a close peer to this role.
    external_comparables:
      grade: A
      comment: The role is at the 50th percentile based on our market data.
    internal_comparables:
      grade: B
      comment: This benchmark is at or above those of comparable roles
    bonus:
      grade: C
      comment: This role has incentive bonuses at the Distinguished/Director level and above, but not at the Staff/Manager level
  recommendation:
    date: 2019-08-08
    recommendations:
      - A cost-of-living increase in the next compensation review.
      - We will try to make the case for an incentive bonus for the Staff/Manager level before the end of 2019.
- slug: support-engineer
  name: Support Engineer
  role_path: /job-families/engineering/support-engineer/
  grades:
    materials:
      grade: B
      comment: The description and career development materials need to be unified. The title is fine.
    recruiting:
      grade: A
      comment: We are attracting top candidates, and are routinely on or above our hiring plan.
    employee_satisfaction:
      grade: B
      comment: There may be sensitivity around the Backend Engineer benchmark being raised in October 2018 because it is a peer to this role.
    external_comparables:
      grade: A
      comment: The role is at the 75th percentile based on our market data.
    internal_comparables:
      grade: B
      comment: This benchmark is at or above those of comparable roles
    bonus:
      grade: C
      comment: This role has incentive bonuses at the Distinguished/Director level and above, but not at the Staff/Manager level
  recommendation:
    date: 2019-04-02
    recommendations:
      - A cost-of-living increase in the next compensation review.
      - We will try to make the case for an incentive bonus for the Staff/Manager level before the end of 2019.
- slug: software-engineer-in-test
  name: Software Engineer in Test
  role_path: /job-families/engineering/software-engineer-in-test/
  grades:
    materials:
      grade: B
      comment: Title is now Software Engineer in Test to reflect the team's skillsets and modern industry standards. We are trailing behind on visionaries where the role is a Developer focused solely on test tools and infrastructure e.g. Google SETIs.
    recruiting:
      grade: B
      comment: We are attracting candidates and routinely on our hiring plan, though we have lost some candidates due to compensation expectations.
    employee_satisfaction:
      grade: B
      comment: Discussions around the title refresh have been brought up by team members. Existing hires have also held this title in their previous job experience.
    external_comparables:
      grade: C
      comment: We are at the 50th percentile based on our compensation data. We are not attracting candidates who hold Software Engineer in Test positions. We have lost candidates in the hiring pipeline.
    internal_comparables:
      grade: B
      comment: There may be sensitivity that there are Backend Engineers in the same department (Quality) who are on a different benchmark.
    bonus:
      grade: C
      comment: This role has incentive bonuses at the Distinguished/Director level and above, but not at the Staff/Manager level
  recommendation:
    date: 2019-04-18
    recommendations:
      - Revisit benchmark with modern Test Engineering titles such as Software Engineer in Test.
      - Change the title to Software Engineer in Test.
      - A cost-of-living increase in the next compensation review.
      - We will try to make the case for an incentive bonus for the Staff/Manager level before the end of 2019.
- slug: security-analyst
  name: Security Analyst
  role_path: /job-families/engineering/security-analyst/
  grades:
    materials:
      grade: B
      comment: The differentiation between levels of this role need better definition.
    recruiting:
      grade: A
      comment: We are attracting great candidates and routinely at or ahead of our plan.
    employee_satisfaction:
      grade: A
      comment: We have heard little-to-no-complaints from individuals in this role about compensation.
    external_comparables:
      grade: B
      comment: We are at the 50th percentile based on our compensation data.
    internal_comparables:
      grade: C
      comment: This benchmark is lower, compared to other Security Department benchmarks.
    bonus:
      grade: D
      comment: We have not had any bonuses for this role
  recommendation:
    date: 2019-05-29
    recommendations:
      - We will try to make the case for an incentive bonus for the Staff/Manager level before the end of 2019
- slug: security-engineer
  name: Security Engineer
  role_path: /job-families/engineering/security-engineer/
  grades:
    materials:
      grade: B
      comment: The description and career development materials need to be unified. The title is fine.
    recruiting:
      grade: A
      comment: We are attracting great candidates and routinely at or ahead of our plan.
    employee_satisfaction:
      grade: A
      comment: We have heard little-to-no-complaints from individuals in this role about compensation.
    external_comparables:
      grade: A
      comment: We are at the 75th percentile based on our compensation data
    internal_comparables:
      grade: A
      comment: This role is well compensated compared to adjecent roles like SRE and Backend Engineer.
    bonus:
      grade: C
      comment: This role has incentive bonuses at the Distinguished/Director level and above, but not at the Staff/Manager level
  recommendation:
    date: 2019-04-02
    recommendations:
      - A cost-of-living increase in the next compensation review.
      - We will try to make the case for an incentive bonus for the Staff/Manager level before the end of 2019
- slug: ux-designer
  name: Product Designer
  role_path: /job-families/engineering/product-designer/
  grades:
    materials:
      grade: A
      comment: New title of Product Designer is attracting the type of experience we're targeting, and career development materials are up to date.
    recruiting:
      grade: A
      comment: We are attracting candidates with the desired qualifications.
    employee_satisfaction:
      grade: A
      comment: We have heard little-to-no complaints from employees
    external_comparables:
      grade: A
      comment: We are at the 85th percentile based on our compensation data.
    internal_comparables:
      grade: A
      comment: This benchmark is at or above those of comparable roles
    bonus:
      grade: C
      comment: This role has incentive bonuses at the Distinguished/Director level and above, but not at the Staff/Manager level
  recommendation:
    date: 2019-04-02
    recommendations:
      - A cost-of-living increase in the next compensation review.
      - We will try to make the case for an incentive bonus for the Staff/Manager level before the end of 2019
- slug: ux-researcher
  name: UX Researcher
  role_path: /job-families/engineering/ux-researcher/
  grades:
    materials:
      grade: B
      comment: The description and career development materials need to be unified. The title is fine.
    recruiting:
      grade: B
      comment: We are meeting our hiring plan with great effort, but we are struggling to attract candidates with the desired qualifications.
    employee_satisfaction:
      grade: B
      comment: Most people are happy, but we can be always be better.
    external_comparables:
      grade: A
      comment: We are at the 85th percentile based on our compensation data.
    internal_comparables:
      grade: A
      comment: This role is above adjacent positions like Frontend Engineer.
    bonus:
      grade: C
      comment: This role has incentive bonuses at the Distinguished/Director level and above, but not at the Staff/Manager level
  recommendation:
    date: 2019-04-02
    recommendations:
      - A cost-of-living increase in the next compensation review.
      - We will try to make the case for an incentive bonus for the Staff/Manager level before the end of 2019
- slug: site-reliability-engineer
  name: Site Reliability Engineer
  role_path: /job-families/engineering/site-reliability-engineer/
  grades:
    materials:
      grade: B
      comment: The description and career development materials need to be unified. The title is fine.
    recruiting:
      grade: A
      comment: We are attracting top candidates amd are on or ahead of our hiring plan.
    employee_satisfaction:
      grade: A
      comment: There have been litte-to-no complaints since the SRE role replaced the Production Engineer role in 2018
    external_comparables:
      grade: A
      comment: We are at or above the 75th percentile based on external data
    internal_comparables:
      grade: A
      comment: This role is well compensated compared to adjecent roles like Backend Engineer and Security Engineer.
    bonus:
      grade: C
      comment: This role has incentive bonuses at the Distinguished/Director level and above, but not at the Staff/Manager level
  recommendation:
    date: 2019-04-02
    recommendations:
      - A cost-of-living increase in the next compensation review.
      - We will try to make the case for an incentive bonus for the Staff/Manager level before the end of 2019
- slug: database-reliability-engineer
  name: Database Reliability Engineer
  role_path: /job-families/engineering/database-reliability-engineer/
  grades:
    materials:
      grade: B
      comment: The description and career development materials need to be unified. The title is fine.
    recruiting:
      grade: A
      comment: We are attracting top candidates amd are on or ahead of our hiring plan.
    employee_satisfaction:
      grade: A
      comment: There have been litte-to-no complaints since the DBRE role replaced the DBA role in late 2018
    external_comparables:
      grade: A
      comment: We are at or above the 75th percentile based on external data
    internal_comparables:
      grade: A
      comment: This role is well compensated compared to adjecent roles like Site Reliability Engineer and Security Engineer.
    bonus:
      grade: C
      comment: This role has incentive bonuses at the Distinguished/Director level and above, but not at the Staff/Manager level
  recommendation:
    date: 2019-04-02
    recommendations:
      - A cost-of-living increase in the next compensation review.
      - We will try to make the case for an incentive bonus for the Staff/Manager level before the end of 2019
- slug: technical-writer
  name: Technical Writer
  role_path: /job-families/engineering/technical-writer/
  grades:
    materials:
      grade: B
      comment: Title and job descriptions are clear and detailed. Career development artifacts need to be developed.
    recruiting:
      grade: B
      comment: Nearly caught up to plan after being behind due to relying on a senior pipeline exclusively (no intermediate pipeline), and due to the challenge in finding senior candidates with sufficient qualifications. We have improved this by (re-)posting both levels, better promoting openings, and conducting more active recruitment. Compensation has been a problem for several candidates this year. We may need to skew higher than average non-GitLab "technical writer" roles, as the title can bring a large range of technical expertise, and we require more than average.
    employee_satisfaction:
      grade: A
      comment: No complaints and some positive reactions on salary level since the January 2019 update to base salary.
    external_comparables:
      grade: TBD
      comment: Need comparison to external data, which has been requested of Compensation and Benefits.
    internal_comparables:
      grade: B
      comment: Comp is 10% under Support Engineer and 8.5% under Web Content Manager, yet both have commonalities with this role's technical and communication skills. We should probably be close or closer to those roles in salary.
    bonus:
      grade: C
      comment: No bonuses at the Staff/Manager level and below.
  recommendation:
    date: 2019-09-24
    recommendations:
      - Craft career development artifacts.
      - Consider comp baseline in light of forthcoming analysis and existing information.
      - A cost-of-living increase in the next compensation review.
      - We will try to make the case for an incentive bonus for the Staff/Manager level before the end of 2019.

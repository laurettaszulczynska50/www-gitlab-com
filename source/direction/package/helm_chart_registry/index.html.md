---
layout: markdown_page
title: "Category Direction - Helm Chart Registry"
---

- TOC
{:toc}

## Helm Chart Registry

Users or organizations who deploy complex pieces of software towards Kubernetes managed environments depend on a standardized way to automate provisioning those external environments. Helm is the package manager for Kubernetes and helps users define, manage, install, upgrade, and rollback even the most complex Kubernetes application. Helm uses a package format called Charts to describe a set of Kubernetes resources

Helm charts will be easy to create, version, share and publish right within GitLab. This would provide an official and integrated method to publish, control, and version control Helm charts. 

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=Category%3AHelm+Chart+Registry)
- [Overall Vision](https://about.gitlab.com/direction/package/)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/593)

This page is maintained by the Product Manager for Package, Tim Rizzi ([E-mail](mailto:trizzi@gitlab.com))

### Usecases listed

We will integrate Helm charts similar to other package management solutions in GitLab such as [Docker](https://gitlab.com/groups/gitlab-org/-/epics/741), [Maven](https://gitlab.com/groups/gitlab-org/-/epics/437), and [NPM](https://gitlab.com/groups/gitlab-org/-/epics/186) in order to see to the following use cases:

1. Public and private repositories for Helm charts
2. Fine-grained access control
3. Verification of Helm chart integrity through [gitlab-org#486](https://gitlab.com/groups/gitlab-org/-/epics/486)
4. Improved accessibility and high availability through caching and proxification due to [gitlab-org#486](https://gitlab.com/groups/gitlab-org/-/epics/486)
5. Standardized workflow to version control and publish charts making use of GitLab's other services
6. Making metadata concurrently available to make workflows more transparent

## What's Next & Why

With the launch of Helm 3, which is now in [beta](https://helm.sh/blog/helm-3-preview-pt3/), pushing and pulling charts can now be done via OCI Registry. This means that users can now utilize the GitLab Container Registry for hosting Helm charts. [gitlab-#30669](https://gitlab.com/gitlab-org/gitlab/issues/30669) will add support for Helm Charts to the Container Registry. It's important to note, that users are already doing this, but the user interface currently doesn't display accurate metadata.

## Maturity Plan

This category is currently at the "Planned" maturity level, and
our next maturity target is Minimal (see our [definitions of maturity levels](/direction/maturity/)).
Key deliverables to achieve this are:

- [MVC for Helm Chart Registry](https://gitlab.com/gitlab-org/gitlab/issues/30669)

## Competitive Landscape

* [Helm Hub](https://hub.helm.sh/)
* [Artifactory](https://www.jfrog.com/confluence/display/RTF/Helm+Chart+Repositories)
* [Chart museum](https://chartmuseum.com/)
* [Codefresh](https://codefresh.io/features/#Helm)
* [Azure DevOps](https://docs.microsoft.com/en-us/azure/container-registry/container-registry-helm-repos)

Helm Hub is the official Helm charts repository, which is supported by products like Artifactory from Jfrog and by Codefresh. Additionally, Chart museum offers an open sourced self-managed solution, aside from being able to code one yourself with GitLab pages, [Apache](https://medium.com/@maanadev/how-set-up-a-helm-chart-repository-using-apache-web-server-670ffe0e63c7), or by using a GH repo's [raw publishing url](https://hackernoon.com/using-a-private-github-repo-as-helm-chart-repo-https-access-95629b2af27c).

The Azure container registry can be used as a host for Helm chart repositories. With Helm 3 changing the storage backend to container registries, we are evaluating if we can offer the same level of support. 

GitLab should join the open source offering of this capability, and improve upon it with features targeted at our EE offering.

## Top Customer Success/Sales Issue(s)

There are currently no customer success or sales issues. 

## Top Customer Issue(s)

The MVC [gitlab-#30669](https://gitlab.com/gitlab-org/gitlab/issues/30669) is currently the top customer issue. 

## Top Internal Customer Issue(s)

There are currently no internal customer issues. 

## Top Vision Item(s)

[gitlab-#9993](https://gitlab.com/gitlab-org/gitlab/issues/9993) details the need to track which underlying packages a release incorporates and detail any changes in those underlying dependencies. Although not specific to Helm charts, this functionality represents a leap forward in our current offering. 

---
layout: markdown_page
title: "Category Direction - Release Orchestration"
---

- TOC
{:toc}

## Release Orchestration

Release Orchestration is the ability to coordinate complex releases, particularly
across projects, in an efficient way that leverages as-code behaviors as much as
possible, but also recognizes that there are manual steps and coordination points
involving human decisions throughout software delivery in the enterprise. More
specifically, this is managing the kinds of complex enterprise releases for which you'd
have a Release Manager in play, rather than having individual teams continually
deploying independent code to production. 

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ARelease%20Orchestration)
- [Overall Vision](/direction/cicd#release)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/594)
- [Documentation](https://docs.gitlab.com/ee/user/project/releases/index.html)

Interested in joining the conversation for this category? Please join us in our
[public epic](https://gitlab.com/groups/gitlab-org/-/epics/1298) where
we discuss this topic and can answer any questions you may have. Your contributions
are more than welcome.

## Manual Processes vs. Automation

One important consideration for how we want to implement release orchestration at
GitLab is that we want to solve automated, continuous delivery workflows first.
We believe that all enterprises can achieve this, and that GitLab as a whole
is a powerful tool to unlock efficiency within DevOps. As such we avoid adding
manual, non-automated controls to the delivery pipeline. That's not to say we don't
want to hear about problems that are currently being solved with manual process,
or that we just don't want to address them, but we want to work with you to creatively
find ways to solve these with automation. Only if that becomes untenable do we
want to look at other options. Ultimately we believe this is best
 for GitLab and for our users; we'd much rather see  people doing release
management using the data that already exists, by using our tool for day to day actions 
and processes and not having to do any manual work on top of it.

### Compliance and Security in the Release Pipeline

Security, compliance, control and governance of the release pipeline is handled
as part of [Release Governance](/direction/release/release_governance). Secrets
in the pipeline are part of our [Secrets Management](/direction/release/secrets_management) category.

## What's Next & Why

Our top focus is to support ease of use for release management in GitLab. The two main features that will enable this are release generation from `.gitlab-ci.yml` ([gitlab#26013](https://gitlab.com/gitlab-org/gitlab/issues/26013)) and filtering issues in a release ([gitlab#32632](https://gitlab.com/gitlab-org/gitlab/issues/32632)). To support our users deliver software faster with GitLab, we are validating how to add assets to a release ([gitlab#27300](https://gitlab.com/gitlab-org/gitlab/issues/27300)). This connection of releases and aseets will improve the richness of the release page, helping users see the full picture of their release in a single place. 

## Maturity Plan

This category is currently at the "Minimal" maturity level, and
our next maturity target is Viable (see our [definitions of maturity levels](/direction/maturity/)).
We believe that adding capabilities to monitor and edit the release directly from the release page will progress the maturity to Viable as it will connect the issues that the development team is working on for a specific milestone and/or release automatically into the workflow.


Key deliverables to achieve this are:

- [Create draft releases](https://gitlab.com/gitlab-org/gitlab-foss/issues/38105) (Complete)
- [Cross-project environment dashboard](https://gitlab.com/gitlab-org/gitlab/issues/3713) (Complete)
- [Edit Release](https://gitlab.com/gitlab-org/gitlab/issues/26016) (Complete)
- [Association of milestones with releases](https://gitlab.com/gitlab-org/gitlab/issues/29020) (Complete)
- [Filter issues based on release](https://gitlab.com/gitlab-org/gitlab/issues/32632) (12.6)
- [Capture Release actions in the audit log page](https://gitlab.com/gitlab-org/gitlab/issues/32807) (12.7)
- [Release generation from within `.gitlab-ci.yml`](https://gitlab.com/gitlab-org/gitlab/issues/26013) 
- [Auto changelog for release](https://gitlab.com/gitlab-org/gitlab/issues/26015)
 

## Competitive Landscape

Release orchestration tools tend to have great features for managing releases,
as you'd expect; they are built from the ground up as a release management
workflow tool and are very capable in those areas. Our approach to release
orchestration will be a bit different, instead of being workflow-oriented we
are going to approach release orchestration from a publishing point of view.
What this means is instead of building complicated workflows for your releases,
we will focus more on the artifact of the release itself and embedding the
checks and steps into it. In [Gartner's recent review of ARO tools](https://bl-www-5483.about.gitlab-review.app/analysts/gartner-aro19/), the strategy to focus on modern application development and delivery workflows was celebrated and validated. 

An important view for the way we look at the world is [gitlab#3713](https://gitlab.com/gitlab-org/gitlab/issues/3713)
which introduces an environments-based view for managing what's deployed.

 We are conducting a detailed comparison to [XL Release](https://xebialabs.com/products/application-release-orchestration-xl-release/) in  and [Electric Cloud](https://electric-cloud.com/) this can be found in [gitlab#37344](https://gitlab.com/gitlab-org/gitlab/issues/37344). 

## Analyst Landscape

Analysts at this time are looking for more quality of life features that make
a difference in people's daily workflows - how does this make my day better?
By introducing features like [gitlab#26015](https://gitlab.com/gitlab-org/gitlab/issues/26015)
to automatically manage release notes as part of releases, we can demonstrate
how our solution is already capable of doing this.

## Top Customer Success/Sales Issue(s)

In the same vein as [gitlab#26015](https://gitlab.com/gitlab-org/gitlab/issues/26015), issues that will support the ease of use of releases like being able to create a release in the UI ([gitlab#32812](https://gitlab.com/gitlab-org/gitlab/issues/32812)] is an issue of interest to satisfy prospect use cases. 

## Top Customer Issue(s)

Implementing [gitlab#26014](https://gitlab.com/gitlab-org/gitlab/issues/26014),
which makes creation of the release package an inline part of the
`.gitlab-ci.yml`, will make this feature feel much more mature and
production-ready (even if it is already really usable.)

## Top Internal Customer Issue(s)

A lot of interest has been expressed in adding the ability to delete environments ([gitlab#20620](https://gitlab.com/gitlab-org/gitlab/issues/20620) and [gitlab#19724](https://gitlab.com/gitlab-org/gitlab/issues/19724)).


## Top Vision Item(s)

An exciting item for the vision is the ability to create releases as
runbooks via [gitlab#9427](https://gitlab.com/gitlab-org/gitlab/issues/9427).
This will allow non-technical users to create runnable release plans in GitLab,
which can have actions embedded in them which will perform automated parts
of the release.

We are beginning to research linking runbooks to Releases in [gitlab#36994](https://gitlab.com/gitlab-org/gitlab/issues/36994), building out the vision for the Release Page to be a single source of truth for project releases. 

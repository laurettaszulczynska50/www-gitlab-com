---
layout: markdown_page
title: "Category Direction - ChatOps"
---

- TOC
{:toc}

## ChatOps

ChatOps is all about conversation-driven ops. While in a chat channel, team members type commands that some bot is configured to execute through custom scripts. These can wildy vary in functionality as they are highly configurable.

Bringing ops tools into team conversations and using bots to work with key parts of infrastructure allows team to automate tasks and collaborate better, faster, and cheaper.

The next generation of our ChatOps implementation will allow users to have a dedicated interface to configure, invoke, and audit ChatOps actions, doing it in a secure way through RBAC.

We've identified two key aspects that must be included in our offering:

1. ChatOps needs to work out of the box without requiring the customer to put in a lot of work.
2. The commands need to be parameterized. So one could specify additional parameters to an argument, for example `/chatops get cpu <hostname>`. We have this today, but we do require you to write the parser and action script yourself.


- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=chatops)
- [Overall Vision](/direction/configure)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/595)

Interested in joining the conversation for this category? Please join us in our
[public epic](https://gitlab.com/groups/gitlab-org/-/epics/591) where
we discuss this topic and can answer any questions you may have. Your contributions
are more than welcome.

## What's next & why

1. [Introduce pipeline type for chatops-generated pipelines](https://gitlab.com/gitlab-org/gitlab-ce/issues/58187)

We want to measure the usage of this feature to prioritize next steps

## Competitive landscape

It’s *really* hard to make “one-size-fits-most” ChatOps when the driving force behind ChatOps is “my business that works in `X` way has a need to perform `Y` function via chat”, where never between most companies will those variables be the same. No one company has emerged as the leader in this area.

### VictorOps

VictorOps is an incident management platform. Through their slack integration they provide common commands to acknowledge and resolve incidents.

### Cog

Cog provides the ability to build new bot commands in any language allowing users to leverage extensibility and security features.

## Analyst landscape

TBD

## Top Customer Success/Sales issue(s)

TBD

## Top user issue(s)

[Slack Custom Actions for Messages](https://gitlab.com/gitlab-org/gitlab-ee/issues/6154)

## Top internal customer issue(s)

TBD

## Top Vision Item(s)

TBD

---
layout: handbook-page-toc
title: "Account Based Marketing"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## What is account based marketing?
Account-based marketing is a strategic approach to marketing based on account awareness in which an organization considers and communicates with individual prospect or customer accounts as markets of one.  Through a close alignment between sales and marketing we focus on target accounts that fit our ICP or ideal customer profile.  At GitLab, we are at the beginning of our account based marketing efforts and in the process of defining our ICP and aligning our target accounts based on those criteria.

## Where does account based marketing fit within the greater marketing org?
Account based marketing sits next to field marketing and is similarly aligned to sales.  Account based marketing looks at an account or target account as a market of one, versus marketing to the total addressable market.

## What is an ideal customer profile (ICP) and how do we determine what that looks like?
An ideal customer profile is the description of our "perfect" customer company (not individual or end user).  The profile should take into consideration firmographic, environmental and additional factors to develop what our ICP looks like.  The account based marketing team is responsible for the management of this project and is working with a tiger team across the greater organization.  We have engaged TOPO to assist with this process, and you can follow along in the [ICP Epic](https://gitlab.com/groups/gitlab-com/-/epics/210)

## Roles & Responsibilities

**Emily Luehrs**  
*Account Based Marketing Manager*
* **Development**:plan account based marketing strategy, prioritze company ojectives as it aligns with ABM
* **Strategy**: plan, prioritize and manage excution of campaigns
* **Ideal Customer Profile**: acts as project manager for the development of our ICP

**Jenny Tiemann**  
*Sr. Marketing Program Manager*
* **Campaigns**: organize execution, timeline, and campaign tracking

**TBH**  
*Digital Marketing Manager*
* **Campaigns**: develop and manage digital assets and implementation for ABM campaigns

## Tools we use

**Demandbase** 
Targeting and personalization platform which we use to target online ads to companies that fit our ICP and tiered account criteria.  

**TOPO**
Research and advisory firm used by companies to develop and orchestrate their account based strategy.  We will be following their model for developing our ideal customer profile (ICP) and account based orchestration plays.
[TOPO research we are using](https://drive.google.com/drive/folders/1PC9Fqri-_JiJM1107B7k-ejD20gV3CnM?usp=sharing)

## Account Based Marketing workflow and labels in GitLab   

The ABM team works from issues and issue boards. If you are needing our assistance with any project, please open an issue and use the ~Account Based Marketing label anywhere within the GitLab repo. 

The ABM team uses this [global issue board](https://gitlab.com/groups/gitlab-com/-/boards/1409957) and also has the [Account Based Marketing Project](gitlab.com/gitlab-com/marketing/account-based-marketing)

Labels used by the team:  
- `Account Based Marketing`: pulls the issue into the board, and is used to put an issue on the team's radar
- `ABM 30 day`, `ABM 60 day`, & `ABM 90 day`: labels used to track our initial rollout of our ABM program.  These labels are used by the internal team only, but our board is organized so that other can follow along
- `ABM Campaign`: Used to identify account based marketing campaigns
*  `status::plan` `status::WIP` & `status::review` are scoped labels to track campagin progress

## Launching account based marketing at GitLab

#### MVC1: 
MM account list-  As our first iteration (30 day plan), we are targeting in Demandbase an account list of roughly 1,000 midmarket accounts based on a target account list provided by the mid market team.

#### MVC2: 
Enterprise launch- 1 rep and account per region (East, West & PubSec AMER, EMEA and APAC) will be including in our initial test run of an account orchestration plan.  Additionally, there will be a campagin runnning in Demandbase based on a tiered account list of our initial ICP qualifiers.

#### MVC3:
A second iteration of MVC 1 & 2 based on the engagement data we see from those initial campaigns.

## Definitions
**Total addressable Market (TAM)**-
Also called total available market, total addressable market references the revenue opportunity available for a product or service. TAM helps to prioritize business opportunities by serving as a quick metric of the underlying potential of a given opportunity

**Ideal customer profile (ICP)**- 
Ideal customer profile is a description of a company who is the best fit for our solution.  This can include firmographics, environmental and behavioral characteristics.  We use this profile to align our account based marketing efforts

**Target accounts**- 
Accounts that fit our ideal customer profile that we will focus our account base strategy on.  Target accounts are simply accounts that we would like to make customers

**Tiered Accounts**- 
Our account based strategy will include tiering our target accounts based on the following tiers:

* **Tier 1**- 1:1 strategy or accounts that match 100% of our ICP criteria and have a marketing plan customized to their organization

* **Tier 2**- 1:few or accounts that match most, but not all, of our ICP criteria.  These accounts will have a marketing plan based on firmographic or environmental behavior, but based on “like” targets i.e. not customized on a 1:1 basis

* **Tier 3**- 1:many or the remainder of our target accounts that we are marketing to but without the resources and customization of higher tiers.


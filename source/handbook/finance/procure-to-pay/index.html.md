---
layout: handbook-page-toc
title: "Vendor Contracts and Invoice Payment"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Procure to Pay Process

### Requirement Identification
Team members can purchase goods and services on behalf of the company in accordance with the [Signature Authorization Matrix](/handbook/finance/authorization-matrix/) and guide to [Spending Company Money](/handbook/spending-company-money). However, any purchases requiring contracts must first be reviewed by procurement then signed off by a member of the executive team. Be sure to also check out our guide on [Signing Legal Documents](/handbook/finance/authorization-matrix/#signing-legal-documents/).

#### Vendor Code of Ethics
All vendors that GitLab does business with, must legally comply with the [Supplier Code of Ethics](https://about.gitlab.com/handbook/people-group/code-of-conduct/#partner-code-of-ethics). When having discussions with your vendor regarding the contract, make them aware of this requirement.

### Vendor and Contract Approval Workflow

#### Step 1: Create a Confidential Issue

 Create a new confidential issue in the Finance Issue Tracker and select the appropriate Vendor Contract Template. Specific templates associated with each function and department can be found below. **Please ensure that you select the appropriate template. This will ensure the appropriate routing of your request.**

 Follow the steps in the appropriate issue:
 - [Vendor Contract Template](https://gitlab.com/gitlab-com/finance/blob/master/.gitlab/issue_templates/vendor_contracts.md)
   - Contracts related to the following purchase types:
     - SaaS Agreements 
     - SaaS Renewals
     - Consulting Agreements
     - Field Marketing and Events with confidential data being shared




 - [Field Marketing/Events Vendor Contract Template - No Confidential Data](https://gitlab.com/gitlab-com/finance/blob/master/.gitlab/issue_templates/vendor_contracts_fieldmarketing.md) 
    - Template for use by Field Marketing Only 
    - Includes marketing programs, sponsorships, hotels, and professional services that do NOT involve the processing or sharing of data


If you are requesting approval of a new Master Agreement, please fill out the form completely, with all requested information. Procurement will not approve the request if the request is incomplete and missing information.

##### Addendums to Original Contracts

- If the original contract has already been approved, but there is an addendum that does not change pricing, please open up original issue and attach the addendum to be reviewed by legal and signed per the Authorization Matrix. Ensure you tag the Finance Business Partner so he / she is made aware. There is no need to request finance approval.

- If the original contract has already been approved, but there is an addendum that **changes** pricing, please create a new issue and follow the appropriate steps.

#### Step 2: Authorizations

GitLab team-members must get approval from other interested departments such as the department head of your department, finance (budgetary authorization) and in some cases security.

1. Please consult the [Authorization Matrix](/handbook/finance/authorization-matrix/) to determine who must sign off on Functional Approval and Financial Approval.  
2. Approval from Finance Ops and Planning for budgeting.

*For events, campaigns, or other program expenses where it makes sense to quantify actuals at the time they are invoiced to    accounts payable, please include the Campaign Finance Tag as indicated in the issue template.*

3. Consult the [Data Classification Policy](https://docs.google.com/document/d/15eNKGA3zyZazsJMldqTBFbYMnVUSQSpU14lo22JMZQY/edit?usp=sharing) to understand whether your contract will need security review. Any contracts that will share RED or ORANGE data will need security approval prior to signing. Additionally, an annual reassessment of vendor's security posture is performed as part of the contract renewal.
4. Complete a [Data Protection Impact Assessment](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/new?issuable_template=Data%20Protection%20Impact%20Assessment); please note that this will be done in partnership with GitLab's Data Protection Officer and reviewed by Security Compliance during the Security Review.
5. If you are purchasing new software/tools, you also need to get approval from Business Ops.  

Assign the issue to the individuals responsible for the necessary authorizations. Authorizations can be completed concurrently. (Do not assign the issue to the Signer yet.)

*Please note that if any non-public GitLab data will be processed, transmitted, or stored by the vendor, a review of the vendor's information security program is required in order to obtain security approval. The Security Compliance team needs 3 business days to complete this review from the time they receive all necessary documentation from the vendor. Information on what documentation (security reporting) may be requested from vendors as part of a review a security review is outlined within the [3rd Party Vendor Security Review Process ](https://about.gitlab.com/handbook/engineering/security/3rd-party-vendor-security-review.html).

#### Step 4: Procurement Approval and Signing

If the contract cannot be approved, Procurement will partner with legal to provide redlines and begin the terms and conditions negotiations with the Vendor.

Once the contract is approved, Procurement will re-assign to you so you can prepare the contract for signing.

1. Create a pdf of the fully approved contract.
2. Assign your issue to the signer.
3. Send contract to the signer through your team's shared HelloSign login.

#### Step 5: Obtain Vendor Signatures

Send the GitLab-signed pdf to the vendor through HelloSign.

#### Step 6: Upload Fully Executed Contract to ContractWorks.

You will need to upload the fully signed pdf into the folder labeled **01. To Be Standardized**. Legal will then organize the contracts using their [instructions and best practices](/handbook/legal/vendor-contract-filing-process)

If you need access to ContractWorks, please process an access request [here](https://gitlab.com/gitlab-com/access-requests/issues/new?issuable_template=New%20Access%20Request).

### Payment
For complete details on how to obtain payment, please head over to Accounting's [Procure to Pay](/handbook/finance/accounting/#procure-to-pay) page. 

### Accounts Payable Performance Indicator

#### Time to Process Invoices in Tipalti <= 2 business days
The time to submit invoices to the business for approval in Tipalti. The target is <= 2 business days.


### Modern Slavery and Human Trafficking Compliance Program

GitLab condemns exploitation of humans through the illegal and degrading practices of human trafficking, slavery, servitude, forced labor, forced marriage, the sale/exploitation of chilren and adults and debt bondage (“Modern Slavery”).  To combat such illegal activities, GitLab has implemented this Modern Slavery and Human Trafficking Compliance Program.  

*Risk Areas and Markets*
While Modern Slavery can occur in any country and in any market, some regions and sectors present higher likelihood of vioaltions. Geographies with higher incidents of slavery are India, China, Pakistan, Bangladesh, Uzbekistan, Russia, Nigeria, Indonesia and Egypt. Consumer sectors such as food, tobacco and clothing are high risk sectors; but Modern Slavery can occur in many markets.*  
 (*According to Source: Statista, Walk Free Foundation, https://www.statista.com/chart/4937/modern-slavery-is-a-brutal-reality-worldwide/)  

*Actions to Address Modern Slavery Risks*
All vendors, providers and entities providing services or products to GitLab (“Vendors”) are expected to comply with [GitLab’s Partner Code of Ethics](/handbook/people-group/code-of-conduct/#partner-code-of-ethics) which specifically addresses Modern Slavery laws.  Compliance with the Partner Code of Ethics will be included in Vendor contracts and/or purchase orders going forward.  Existing Vendor contracts will be updated with the appropriate language upon renewal.

Those entities who are of higher risk or whom GitLab suspects may be in violation of Modern Slavery laws, may be required to complete an audit.  Audits may be presented in the form of a questionnaire or may be an onsite visit.  Any known or suspected violations will be raised to Legal and/or Compliance.  Failure to comply with Modern Slavery laws will result in a termination of the relationship and GitLab retains all rights in law and equity.

Vendors understand and agree that violations of Modern Slavery laws may require mandatory reporting to governing authorities. GitLab has discretion if and how to best consult with Vendors for purposes of Modern Slavery reporting. GitLab is senstive to and will take into consideration, the relationship and the risk profile of Vendor to ensure that Modern Slavery risks have been appropriately identified, assessed and addressed and that the Vendor is aware of what actions it needs to take.

*Assessment of Effectiveness*
GitLab will review its Modern Slavery and Human Trafficking Compliance Program on an annual basis or sooner if it is determined there is increased exposure or concerns with overall compliance.   The Program may be amended from time to time by GitLab, to ensure compliance with the most current Modern Slavery laws and regulations.

*Compliance Program Approval*
GitLab’s Executive Team reviewed and approves this Modern Slavery and Human Trafficking Compliance Program.

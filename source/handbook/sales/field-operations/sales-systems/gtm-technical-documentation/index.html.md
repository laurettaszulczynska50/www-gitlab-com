---
layout: handbook-page-toc
title: "Go-To-Market Technical Documentation"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## How to use this documentation

The Documentation below is organized by Feature, each section will have the relevent inputs and outputs as well as references to the logic that processes the input and outputs.

---

## Territory Success Planning

**Business Process this supports:** [Territory Success Planning](https://about.gitlab.com/handbook/sales/field-operations/sales-operations/go-to-market/#territory-success-planning-tsp)

**Overview:** The goal of TSP is to keep a set of staging fields constantly update to date from a variety of data sources, then at given intervals copy these vales to the "Actual" set of fields for general use. This allows for us to contantly receive changes but only apply those changes in a control fashion at given intervals. This also allows us to easily track exceptions. Note: This project was orginally referred to as ATAM, which is why the API names of the fields reference that instead of TSP.

**Logic Locations:** [AccountJob.cls](https://gitlab.com/gitlab-com/sales-team/field-operations/salesforce-src/blob/master/force-app/main/default/classes/AccountJob.cls)
Code Units:
* getHighestEmployeesInFamily
* stampATAMAddressFields

**Inputs:** DataFox, DiscoverOrg, Manually Entered Address & Employee Data, Account Parenting Hierarchy

**Outputs:** Here is the outline between of two sets of fields we are setting on the Account object. Staging(TSP / ATAM) are set nightly via APEX job. Actuals are set at given intervals found in the business documentation.

| **Data Name**     | **Actual - Field API Name**                  | **TSP - Field API Name**        |
|---------------|--------------------------------------------|-----------------------------|
| Owner         | Owner                                      | ATAM_Approved_Next_Owner__c |
| Owner Role    | Owner.Role                                 | ATAM_Next_Owner_Role__c     |
| Owner Team    | Account_Owner_Team__c                      | ATAM_Next_Owner_Team__c     |
| Max Employees | TBD                                        | JB_Max_Family_Employees__c  |
| Sales Segment | Ultimate_Parent_Sales_Segment_Employees__c | JB_Test_Sales_Segment__c    |
| Region        | Region__c                                  | ATAM_Region__c              |
| Sub-Region    | Sub_Region__c                              | ATAM_Sub_Region__c          |
| Area          | Account_Area_ADMIN_USE_ONLY__c             | ATAM_Area__c                |
| Territory     | Account_Territory__c                       | ATAM_Territory__c           |
| Country       | TBD                                        | ATAM_Address_Country__c     |
| State         | TBD                                        | ATAM_Address_State__c       |
| City          | TBD                                        | ATAM_Address_City__c        |
| Street        | TBD                                        | ATAM_Address_Street__c      |
| Postal Code   | TBD                                        | ATAM_Address_Postal_Code__c |

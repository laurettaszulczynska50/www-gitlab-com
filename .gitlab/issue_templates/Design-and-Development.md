<!--Request to create a new page/flow or to update an existing one-->
<!--Please remember this are suggestions. Any extra information you provide is beneficial.-->
<!-- Edit this file at : https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/.gitlab/issue_templates/Design-and-Development.md -->
<!-- Template code owner: @brandon_lyon, marketing web designer/developer -->

### Briefly describe the page/flow

(Example: Add a new page comparing us to a specific competitor)

### If applicable, do you have any relevant wireframes, layouts, content/data?

(Example: Google doc/sheet, balsamiq, etc)

### What is the primary purpose of this page/flow?

(Example: Increase EE trials by 5%)

### What other purposes does this page/flow serve, if any?

(Example: Capture emails for campaign funnels)

### What information is this page/flow trying to convey?

(Example: Forrester says GitLab is great)

## Who is the primary audience? Be specific.

(Example: Potential customers vs developers vs CTO vs Jenkins users)

### Any other audiences?

(Example: Media outlets, investors, )

### Where do you expect traffic to come from?

(Example: Homepage direct link, on-site blog post, social media campaign, targeted ads)

### If applicable, are there any items which can be hidden at first glance but still need to be present?

(Example: Tooltips, collapsible sections, filterable items, different view-type toggles, view-more buttons, extra data)

### Please provide some example websites for design guidance.

(Example: "Pinterest implements recommendations well", "Stripe has good payment UX", "I like the aesthetics of nike.com", animation examples, microinteractions, etc)

<!-- Preexisting Pages Only -->

### Where/how do you feel the page can be improved?

(Example: Performance is slow, page isn't navigable on mobile devices, I can't find the data I'm looking for)

### Does anyone have usage metrics for this page? Click data? Heatmaps?

(If yes, then who/what? If no, then why not?)

### Do we use or need all of the information currently presented on the page?

(Information overload is real. Visual chaos is real. Try to keep things minimal.)

### Are the currently existing graphs/items meaningful (relevant, readable, appropriately framed, etc)? How would you like to see them improved?

Example: Make the colors accessible, font sizes are hard to read, this should be a bar graph instead of a line graph)

### Is there anything currently missing from the page that you would like to see added to it?

(Example: Links to new content, graphs/data, illustrations, video, tutorials)

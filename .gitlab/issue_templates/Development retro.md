Please review and update issues/epics/MRs from the last release retro:

Issues: https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=retrospective%3A12.X

Epics: 
https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=retrospective%3A12.X

MRs:
https://gitlab.com/groups/gitlab-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=retrospective%3A12.X

Untracked via label because it’s outside labels

Please have a retrospective with your team following the guidelines outlined in the handbook here: https://about.gitlab.com/handbook/engineering/management/#team-retrospectives and https://about.gitlab.com/handbook/engineering/management/team-retrospectives/.  

After the retrospective is complete, please select some of your learnings to share company-wide in the retro doc: https://docs.google.com/document/d/1nEkM_7Dj4bT21GJy0Ut3By76FZqCfLBmFQNVThmW2TY/edit

For items which have didn't go well, create an issue to address labeled with 'retrospective X.Y'.  In the case where a manager feels an issue can/should not be created, please include that in the what went wrong section.

Please try to group by *topic* rather than by *team*, as suggested in #3416. Let's try to make sure our entries are in as soon as possible so we can start prepping for a smoother retrospective.

| Team                | Manager         | Retro done?        | Doc updated?       |
| ------------------- | --------------- | :----------------: | :----------------: |
| Configure:Orchestration | @sengelhard + @jeanduplessis   |  |  |
| Configure:System    | @nicholasklick + @jeanduplessis |  |  |
| Create:Source Code  | @m_gill + @andr3  |  |  |
| Create:Editor       | @andr3 + @dsatcher |  |  |
| Create:Knowledge    | @andr3 + @dsatcher |  |  |
| Distribution        | @mendeni          |  |  |
| Fulfillment         | @jameslopez + @chris_baus  |||
| Geo                 | @rnienaber      |  |  |
| Gitaly              | @zj-gitlab  |  |  |
| Manage              | @lmcandrew + @dennis + @djensen     |  | |
| Monitor:APM         | @mnohr + @adriel |  |  |
| Monitor:Health      | @sengelhard + @ClemMakesApps |  |  |
| Memory              | @craig-gomes |  |  |
| Search              | @changzhengliu  |  |  |
| Database            | @craig-gomes |  |  |
| Plan                | @smcgivern + @donaldcook + @johnhope |   |  |
| Package             | @dcroft |  |  |
| Quality             | @meks           |  | |
| Secure              | @leipert + @twoodham + @gonzoyumo + @sethgitlab |  |  |
| UX                  | @clenneville    |  |  |
| Verify:CI           | @crystalpoole + @dcipoletti  |  |  |
| Verify:Testing      | @erushton + @dcipoletti  |  |  | 
| Verify:Runner       | @erushton + @dcipoletti  |  |  | 
| Release             | @darbyfrey + @jhampton |  |  |
| Ecosystem           | @nhxnguyen |  |  | 
| Acquisition and Conversion | @jeromezng  |  |  |
| Expansion and Retention | @pcalder |  |  |
| Defend              | @twoodham |  |  |
